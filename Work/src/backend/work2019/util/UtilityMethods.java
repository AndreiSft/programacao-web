package backend.work2019.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class UtilityMethods {
	private static EntityManagerFactory factory;
	private static EntityManager entityManager;

	public static EntityManager playConnection() {
		if (factory == null) {
			factory = Persistence.createEntityManagerFactory("persistenceUnitName");
		}

		entityManager = factory.createEntityManager();

		return entityManager;
	}

	public static void stopConnection() {
		if (entityManager != null && entityManager.isOpen()) {
			entityManager.close();
		}
	}
}
