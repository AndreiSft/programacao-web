package backend.work2019.business;

import backend.work2019.domain.Pessoa;
import backend.work2019.persistence.PessoaDAO;

public class PessoaBO {
	private PessoaDAO pessoaDAO = new PessoaDAO();

	public void addPessoa(Pessoa pessoa) throws Exception {
		validate(pessoa);
		pessoaDAO.addPessoa(pessoa);
	}

	private void validate(Pessoa pessoa) throws Exception {
		if (pessoaDAO.validate(pessoa) == true) {
			throw new Exception("");
		}
	}
}
