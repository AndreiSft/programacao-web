package backend.work2019.domain;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import backend.work2019.business.PessoaBO;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet(urlPatterns = "/RegisterServlet")
public class RegisterServlet extends HttpServlet {

	private static final long serialVersionUID = 8758865573961703697L;
	private PessoaBO pessoaBO = new PessoaBO();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setCharacterEncoding("utf-8");
//		response.getWriter().print(addPessoa(request));
		response.getWriter().print("Ol�");
	}

	private String addPessoa(HttpServletRequest request) {
		String nome = request.getParameter("nome");
		Integer rg = Integer.parseInt(request.getParameter("rg"));
		String cpf = request.getParameter("cpf");
		String sexo = request.getParameter("sexo");
		String email = request.getParameter("email");
		String senha = request.getParameter("senha");
		
		Pessoa pessoa = new Pessoa(nome, rg, cpf, sexo, email, senha);

		try {// provis�rio
			pessoaBO.addPessoa(pessoa);
			return "Cadastro Efetuado!";
		} catch (Exception e) {
			return e.getMessage();
		}
	}

}
