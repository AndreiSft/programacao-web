package backend.work2019.persistence;

import javax.persistence.EntityManager;

import backend.work2019.domain.Pessoa;
import backend.work2019.util.UtilityMethods;

public class PessoaDAO {
	
	EntityManager entityManager = UtilityMethods.playConnection();
	
	public void addPessoa(Pessoa pessoa) {
		entityManager.getTransaction().begin();
		entityManager.persist(pessoa);
		entityManager.getTransaction().commit();
		UtilityMethods.stopConnection();
	}
	
	public void removePessoa(Pessoa pessoa) {
		entityManager.getTransaction().begin();
		entityManager.remove(pessoa);
		entityManager.getTransaction().commit();
		UtilityMethods.stopConnection();
	}
	
	public void consultPessoa() {
		
	}
	
	public void editPessoa() {
		
	}
	
	public boolean validate(Pessoa pessoa) {
		return true;//provisório
	}
}
