let nome = document.forms.nome;
let rg = document.forms.rg;
let cpf = document.forms.cpf;
let senha = document.forms.senha;
let senha_confirm = document.forms.senha_confirm;
let idade = document.forms.idade;
let sexo = document.forms.sexo;
let email = document.forms.email;

let nome_error = document.getElementById("nome_error");
let rg_error = document.getElementById("rg_error");
let cpf_error = document.getElementById("cpf_error");
let senha_error = document.getElementById("senha_error");
let idade_error = document.getElementById("idade_error");
let sexo_error = document.getElementById("sexo_error");
let email_error = document.getElementById("email_error");

nome.addEventListener("blur", nameVerify, true);
rg.addEventListener("blur", rgVerify, true);
cpf.addEventListener("blur", cpfVerify, true);
senha.addEventListener("blur", senhaVerify, true);
senha_confirm.addEventListener("blur", senhaTwoVerify, true);
idade.addEventListener("blur", idadeVerify, true);
sexo.addEventListener("blur", sexoVerify, true);
email.addEventListener("blur", emailVerify, true);

function validate() {
	if (nome.value == "") {
		nome.style.border = "2px solid red";
		nome_error.textContent = "Name is required";
		nome.focus();
		return false;
	}

	if (rg.value == "") {
		rg.style.border = "2px solid red";
		rg_error.textContent = "Rg is required";
		rg.focus();
		return false;
	} else if (rg.value.length < 11) {
		rg.style.border = "2px solid red";
		rg_error.innerHTML = "O mínimo é 11 digitos";
		return false;
	}

	if (cpf.value == "") {
		cpf.style.border = "2px solid red";
		cpf_error.textContent = "Cpf is required";
		cpf.focus();
		return false;
	} else if (cpf.value.length < 11) {
		cpf.style.border = "2px solid red";
		cpf_error.innerHTML = "O min é 11 digitos";
		return false;
	}

	if (senha.value == "") {
		senha.style.border = "2px solid red";
		senha_error.textContent = "password is required";
		senha.focus();
		return false;
	} else if (senha.value.length < 8) {
		senha.style.border = "2px solid red";
		senha_error.innerHTML = "min 8 digitos";
		return false;
	} else if (senha_confirm.value == "") {
		senha_confirm.style.border = "2px solid red";
		senha_two_error.textContent = "password is required";
		senha_confirm.focus();
		return false;
	}

	if (senha.value != senha_confirm.value) {
		senha.style.border = "2px solid red";
		senha_confirm.style.border = "2px solid red";
		senha_two_error.innerHTML = "the two passwords do not match"
		return false;
	}

	if (email.value == "") {
		email.style.border = "2px solid red";
		email_error.textContent = "email is required";
		email.focus();
		return false;
	}
}

function nameVerify() {
	if (name.value != "") {
		nome.style.border = "none";
		nome_error.innerHTML = "";
		return true;
	}
}

function rgVerify() {
	if (rg.value != "") {
		rg.style.border = "none";
		rg_error.innerHTML = "";
		return true;
	}
}

function cpfVerify() {
	if (cpf.value != "") {
		cpf.style.border = "none";
		cpf_error.innerHTML = "";
		return true;
	}
}

function senhaVerify() {
	if (senha.value != "") {
		senha.style.border = "none";
		senha_error.innerHTML = "";
		return true;
	}
}

function senhaTwoVerify() {
	if (senha_confirm.value != "") {
		senha_confirm.style.border = "none";
		senha_two_error.innerHTML = "";
		return true;
	}
}

function onlyNumber(e) {
	let charCode = e.charCode ? e.charCode : e.KeyCode;
	// charCode 8 = backspace
	// charCode 9 = tab
	if (charCode != 8 && charCode != 9) {
		// charCode 48 equivale a 0
		// charCode 57 equivale a 9
		if (charCode < 48 || charCode > 57) {
			return false;
		}
	}
}

function onlyChar() {
	tecla = event.keyCode;
	if (tecla >= 33 && tecla <= 64 || tecla >= 91 && tecla <= 93
			|| tecla >= 123 && tecla <= 159 || tecla >= 162 && tecla <= 191) {
		return false;
	} else {
		return true;
	}
}
